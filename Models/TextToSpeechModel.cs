﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class TextToSpeechModel
    {
        public string id { get; set; }
        public List<String> message { get; set; }
    }
}
