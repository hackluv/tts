﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class MessageTextToSpeech
    {
        public string async { get; set; }
        public int error { get; set; }
        public string message { get; set; }
        public string request_id { get; set; }
    }
}
