﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class MessageCallBack
    {
        public string message { get; set; }
        public string success { get; set; }
    }
}
