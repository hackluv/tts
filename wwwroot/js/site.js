﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code

/**
 * Chuyển đổi html thành định dạng plain text
 * @param {string} el (HTML)
 */

function read() {
    var element_variable = "#detail-item .content";
    var element = document.querySelector(element_variable);
    $("img").remove;
    var text = getInnerText(element);
    countAndSplitWord(text);
}

function getInnerText(el) {
    var sel, range, innerText = "";
    if (typeof document.selection != "undefined" && typeof document.body.createTextRange != "undefined") {
        range = document.body.createTextRange();
        range.moveToElementText(el);
        innerText = range.text;
    } else if (typeof window.getSelection != "undefined" && typeof document.createRange != "undefined") {
        sel = window.getSelection();
        sel.selectAllChildren(el);
        innerText = "" + sel;
        sel.removeAllRanges();
    }
    return innerText;
}

/**
 * tính số lượng ký tự và sau đó chia cho 5000 ( giới hạn văn bản của FPT Text to Speech)
 * @param {string} str ( văn bản sau khi chuyển từ HTML sang plain text)
 */
function countAndSplitWord(str) {
    var count = 0;
    var start = 0
    var end = 4999;
    var strSplit = "";
    var contents = [];

    if (str.length % 5000 == 0) {
        count = str.length / 5000;
    } else if (str.length % 5000 != 0) {
        count = parseInt((str.length / 5000)) + 1
    }

    var nLength = 0;

    for (i = 1; i <= count; i--) {
        strSplit = str.slice(start, end);
        start = end + 1;
        end = end + 4999;
        if (count > 1) {
            nStart = end + 1;
            nEnd = end + 4999;
            nStrSplit = str.slice(start, end);
            nLength = filterWord(strSplit, nStrSplit);
            if (nLength > 0) {
                end = newEndLength(end, nLength);
                strSplit = newText(str, start, end);
                contents.push(strSplit);
                count--;
            } else {
                contents.push(strSplit);
                count--;
            }
        }
        if (count == 1) {
            strSplit = str.substr(start, str.length - 1);
            contents.push(strSplit);
            count--;
        }
        sendTextToServer(contents);
        if (count <= 0) {
            break;
        }
    }
}

/**
 * lọc ký tự theo vị trí đầu tiên của đoạn tiếp theo ( để tránh trường hợp dư hoặc thiếu chữ)
 * nếu đủ thì trả về 0
 * thừa thiếu ký tự sẽ trả về số lượng ký tự để giảm bắt ở đoạn đầu tiên.
 * @param {string} str ( đoạn văn bản thứ nhất)
 * @param {string} strSplit ( đoạn văn bản thứ 2)
 */
function filterWord(strSplit1, strSplit2) {
    var arrStr = strSplit1.split(' ');
    if (checkCharacter(strSplit2.charAt(0)) == false) {
        return arrStr[arrStr.length - 1].length;
    } else {
        return 0;
    }
}

/**
 * Kiểm tra ký tự nếu trùng thì trả về true
 * không trả về false
 * @param {char} char
 */
function checkCharacter(char) {
    switch (char) {
        case ' ':
            return true;
            break;
        case ',':
            return true;
            break;
        case '.':
            return true;
            break;
        case '-':
            return true;
            break;
        default:
            return false;
            break;
    };
}

function newEndLength(end, nLength) {
    return end - nLength;
}

function newText(str, start, end) {
    return str.slice(start, end);
}

function sendTextToServer(list) {
    const BASE_URL = "https://localhost:44395/";
    const URI = "TTS/TextToSpeechAction";
    var json = JSON.stringify({ "id": getUri(), "message": list });
    $.ajax({
        url: BASE_URL + URI,
        method: "POST",
        data: json,
        contentType: "application/json",
        success: function (data) {
            var obj = JSON.parse(data);
            var url = obj["url"];
            play(url);
        },
        error: function (errMsg) {
            alert(JSON.stringify(errMsg));
        }
    });
}

function getUri() {
    var current_url = "http://baria.baria-vungtau.gov.vn/article/?item=63118a8adb9bf5623ef24576406c524e";
    //var current_url = window.location;
    var uri = new URL(current_url);
    return uri.search.split("=")[1];
}

function play(url) {
    var audio = document.getElementById('audio-player');
    audio.src = url;
    audio.controls = true;
    audio.play();
}