﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class TTSController : Controller
    {
        const string API_KEY = "YzWiG6nasgHoBgJzAfJl77st9xbGW5fh";
        const string VOICE_ACT = "linhsan";
        const string API_TTS_URL = "https://api.fpt.ai/hmi/tts/v5";
        const string CALL_BACK_URL = "https://localhost:44395/TTS/CallBackMessage";
        const string SPEED = "-1";
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public TTSController(IHostingEnvironment hostingEnvironment, IHttpContextAccessor httpContextAccessor)
        {
            _hostingEnvironment = hostingEnvironment;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpGet]
        public IActionResult TextToSpeechAction()
        {
            return View();
        }

        [HttpPost]
        public IActionResult TextToSpeechAction([FromBody] TextToSpeechModel model)
        {
            string hosting = _httpContextAccessor.HttpContext.Request.Host.Value;

            var isExist = checkExistFile(model.id);
            if (isExist.Length == 0 && isExist == null)
            {
                var path = CreateFolder(model.id);
                var contents = model.message;
                for (var i = 0; i < contents.Count; i++)
                {
                    var result = TextToSpeech(contents[i]);
                    MessageTextToSpeech message = JsonConvert.DeserializeObject<MessageTextToSpeech>(result);
                    if (message.error != 0)
                    {
                        return Json(message);
                    }
                    else
                    {
                        SaveFile(message.async, path, i);
                    }
                }
                MergeAndRenameMp3File(path, model.id);
                return Json("{\"url\":" + "\"" + _httpContextAccessor.HttpContext.Request.Scheme + @"://" + hosting + @"/media/" + isExist + "\"}");
            }
            else
            {
                return Json("{\"url\":" + "\"" + _httpContextAccessor.HttpContext.Request.Scheme + @"://" + hosting + @"/media/" + isExist + "\"}");
            }
        }

        private string TextToSpeech(string text)
        {
            return Task.Run(async () =>
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("api-key", API_KEY);
                client.DefaultRequestHeaders.Add("speed", SPEED);
                client.DefaultRequestHeaders.Add("voice", VOICE_ACT);
                client.DefaultRequestHeaders.Add("callback_url", CALL_BACK_URL);
                var response = await client.PostAsync(API_TTS_URL, new StringContent(text));
                return await response.Content.ReadAsStringAsync();
            }).GetAwaiter().GetResult(); ;
        }

        private string CreateFolder(string id)
        {
            string wwwrootPath = _hostingEnvironment.WebRootPath;
            string folderMedia = "\\media\\";
            string fullPath = wwwrootPath + folderMedia + id;
            try
            {
                if (Directory.Exists(fullPath))
                {
                    return fullPath;
                }
                else
                {
                    Directory.CreateDirectory(fullPath);
                    return fullPath;
                }
            }
            catch
            {
                return "";
            }
        }

        private async Task<bool> SaveFile(string url, string path, int count)
        {
            Uri uri = new Uri(url);
            string name = Path.GetFileName(uri.AbsolutePath);
            using (WebClient webClient = new WebClient())
            {
                webClient.DownloadFile(url, path + "/" + count + "-" + name);
                return true;
            }
        }

        private string checkExistFile(string name)
        {
            string newPath = _hostingEnvironment + @"\media\";
            var result = Path.GetFileName(newPath + name + ".mp3");
            if (result.Length == 0 || result == null)
            {
                return "";
            }
            else
            {
                return result;
            }
        }

        private bool MergeAndRenameMp3File(string path, string id)
        {
            try
            {
                string targetPath = _hostingEnvironment.WebRootPath + @"\media\";
                string newPath = path.Replace(@"\\", @"\");
                DirectoryInfo directory = new DirectoryInfo(newPath);
                var files = directory.GetFiles("*.mp3").ToList();
                if (files.Count() == 1)
                {
                    System.IO.File.Move(newPath + @"\" + files[0].FullName, targetPath + @"\" + id + ".mp3");
                    return true;
                }
                else if (files.Count() >= 2)
                {
                    string[] filesInDir = Directory.GetFiles(newPath);
                    using (var w = new BinaryWriter(System.IO.File.Create(targetPath + @"\" + id + ".mp3")))
                    {
                        new List<string>(filesInDir).ForEach(f => w.Write(System.IO.File.ReadAllBytes(f)));
                    }

                    System.IO.Directory.Delete(newPath, true);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        [HttpPost]
        public bool CallBackMessage([FromBody] string value)
        {
            MessageCallBack callBackMessage = JsonConvert.DeserializeObject<MessageCallBack>(value);
            if (callBackMessage.success.Equals("false"))
            {
                System.Diagnostics.Debug.WriteLine(callBackMessage.message);
                return false;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(callBackMessage.message);
                return true;
            }
        }
    }
}
